// Copyright (C) Bob Trower 1986-2019
// License: 0BSD -- https://opensource.org/licenses/0BSD

#include <stdio.h> 

void showuse()
{
	printf( " beep     (sound beep using ^G)       Bob Trower 08/03/01 \n" );
	printf( "          (C) Copr Bob Trower 1986-2019.    Version 0.00A \n" );
	printf( " Usage:   beep [option]\n" );
	printf( " Options: -h Show this help text\n" );
	printf( "          -v Display ^G text\n" );
	printf( " Purpose: This attempts to sound a beep by printing\n" );
	printf( "          ^G (ASCII 7) to the console.\n" );
	printf( " Release: 0.00.00, Thu Sep 2019 23:45:25 2001, ANSI-C Src\n" );
}

void beep(int displayG) 
{
	char *GString=".";
	*GString=7;
	if(displayG) {
		printf( "^G" ); 
	}
	printf( "%s\n", GString );
}
 
#define displaytxt 1
#define displayoff 0
	
int main( int argc, char**argv ) 
{
	if(argv[1] != NULL) {
		char opt = argv[1][1] ? argv[1][1] : 0;
		switch(opt) {
			case 'h' :
				showuse();
				break;
			case 'v' :
				beep(displaytxt);
				break;
		    default : 
				beep(displayoff);
				break;
		}
	} else {
		beep(displayoff);
	}
} 
